#include <HeightMap.h>

#include <stdio.h>
#include <stdlib.h>

namespace PS2
{

HeightMap::HeightMap() :
	mPositions(nullptr),
    mPositionsSize(0),
	mHeights(nullptr),
    mHeightsSize(0),
	mTexCoords(nullptr),
    mTexCoordsSize(0),
	mIndices(nullptr),
    mIndicesSize(0)
{}

HeightMap::~HeightMap()
{
    if (mPositions != nullptr) { free(mPositions); }
    if (mHeights != nullptr) { free(mHeights); }
    if (mTexCoords != nullptr) { free(mTexCoords); }
    if (mIndices != nullptr) { free(mIndices); }
}

bool HeightMap::Load(
    const char* rawFileName,
	const unsigned int width,
	const float heightScale,
	const float sizeScale
)
{
    mWidth = width;

    FILE* rawFile = fopen(rawFileName, "rb");
    if (!rawFile) {
        printf("Failed to open heighmap raw: %s\n", rawFileName);
        return false;
    }

    uint8_t* fileBytes = nullptr;
    size_t fileBytesSize = 0;
    fseek(rawFile, 0, SEEK_END);
    fileBytesSize = ftell(rawFile);
    fseek(rawFile, 0, SEEK_SET);
    fileBytes = (uint8_t*)malloc(fileBytesSize);
    if (!fileBytes) {
        printf("Failed to alloc file bytes\n");
        fclose(rawFile);
        return false;
    }
    fread((void*)fileBytes, fileBytesSize, 1, rawFile);
    if (fileBytesSize != width*width) {
        printf("File size bytes != width*width\n");
        free(fileBytes);
        fclose(rawFile);
        return false;
    }

    fclose(rawFile);

    mHeights = (float*)malloc(width * width * sizeof(float));
    if (!mHeights) {
        printf("Failed to allocate heights\n");
        free(fileBytes);
        return false;
    }
    mHeightsSize = width * width;
    for (size_t i = 0; i < mHeightsSize; ++i) {
        mHeights[i] = (((float)fileBytes[i]) / 256.0f) * heightScale;
    }

    free(fileBytes);

    if (!GenerateVertices(sizeScale)) { return false; };
    if (!GenerateTexCoords()) { return false; }
    if (!GenerateIndices()) { return false; }

    if (!GenerateBuffers()) { return false; }

    return true;
}

bool HeightMap::GenerateVertices(const float sizeScale)
{
    size_t i = 0;
    mPositions = (Vec4*)memalign(16, mWidth * mWidth * sizeof(Vec4));
    if (!mPositions) {
        printf("Failed to alloc mPositions\n");
        return false;
    }
    mPositionsSize = mWidth * mWidth;

    for (float z = (float)(-((int)mWidth)/ 2); z <= (int)mWidth / 2; ++z) {
        for (float x = (float)(-((int)mWidth) / 2); x <= (int)mWidth / 2; ++x) {
            if (i >= mPositionsSize) {
                printf("Invalid mPositions i index\n");
                return false;
            }

            mPositions[i].x = x;
            mPositions[i].y = mHeights[i];
            mPositions[i].z = z;
            mPositions[i].w = 1.0f;

            // TODO
            // if (x < minX) ...

            ++i;
        }
    }

    // done with heights now, we can free the memory
    free(mHeights);
    mHeights = nullptr;
    mHeightsSize = 0;

    return true;
}

bool HeightMap::GenerateTexCoords()
{
    mTexCoords = (Vec4*)memalign(16, mWidth * mWidth * sizeof(Vec4));
    if (!mTexCoords) {
        printf("Failed to alloc tex coords\n");
        return false;
    }
    mTexCoordsSize = mWidth * mWidth;

    size_t i = 0;
    for (int z = 0; z < mWidth; ++z) {
        for (int x = 0; x < mWidth; ++x) {
            float s = ((float)x / (float)mWidth) * 8.0f;
            float t = ((float)z / (float)mWidth) * 8.0f;
            while (s >= 1.0f) { s -= 1.0f; }
            while (t >= 1.0f) { t -= 1.0f; }
            mTexCoords[i].x = s;
            mTexCoords[i].y = t;
            mTexCoords[i].z = 1.0f; // q
            mTexCoords[i].w = 0.0f;

            ++i;
        }
    }

    return true;
}

bool HeightMap::GenerateIndices()
{
    /*
        We loop through building the triangles that
        make up each grid square in the heightmap
        (z*w+x) *----* (z*w+x+1)
                |   /| 
                |  / | 
                | /  |
        ((z+1)*w+x)*----* ((z+1)*w+x+1)
    */
    mIndices = (int*)malloc(6 * (mWidth - 1) * (mWidth - 1) * sizeof(int));
    if (!mIndices) {
        printf("Failed to alloc indices\n");
        return false;
    }

    mIndicesSize = 6 * (mWidth - 1) * (mWidth - 1);
    
    size_t i = 0;
    for (int z = 0; z < mWidth - 1; ++z) {
        for (int x = 0; x < mWidth - 1; ++x) {
            mIndices[i++] = z * mWidth + x;
            mIndices[i++] = (z+1) * mWidth + x;
            mIndices[i++] = z * mWidth + x + 1;

            mIndices[i++] = (z+1) * mWidth + x;
            mIndices[i++] = (z+1) * mWidth + x + 1;
            mIndices[i++] = z * mWidth + x + 1;
        }
    }

    return true;
}

bool HeightMap::GenerateBuffers()
{
    mVertBuff.Init(
        (float*)mPositions,
        nullptr,
        nullptr,
        (float*)mTexCoords,
        mPositionsSize,
        mIndices,
        mIndicesSize
    );

    return true;
}

} // namespace PS2


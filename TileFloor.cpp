#include <TileFloor.h>
#include <stdio.h>

#define TILESIZE 1.0f

namespace PS2
{

Vec4 TileFloor::sVertices[6] = {
    {  -TILESIZE, 0.0f, TILESIZE, 1.0f }, // bottom left
    {  TILESIZE, 0.0f, TILESIZE, 1.0f }, // bottom right
    {  TILESIZE, 0.0f,-TILESIZE, 1.0f }, // top right
    
    {  -TILESIZE, 0.0f, TILESIZE, 1.0f }, // bottom left
    {  TILESIZE, 0.0f,-TILESIZE, 1.0f }, // top right
    {  -TILESIZE, 0.0f, -TILESIZE, 1.0f }  // top left
};
/*Vec4 TileFloor::sVertices[6] = {
    { -TILESIZE, 0.0f,-TILESIZE, 1.0f },  // top left
    {  TILESIZE, 0.0f,-TILESIZE, 1.0f }, // top right
    { -TILESIZE, 0.0f, TILESIZE, 1.0f }, // bottom left

    {  TILESIZE, 0.0f,-TILESIZE, 1.0f }, // top right
    { -TILESIZE, 0.0f, TILESIZE, 1.0f }, // bottom left
    {  TILESIZE, 0.0f, TILESIZE, 1.0f }  // bottom right
};*/

// STQ
Vec4 TileFloor::sTexCoords[6] = {
    { 0.0f, 0.0f, 1.0f, 0.0f }, // bottom left
    { 1.0f, 0.0f, 1.0f, 0.0f }, // bottom right
    { 1.0f, 1.0f, 1.0f, 0.0f }, // top right

    { 0.0f, 0.0f, 1.0f, 0.0f }, // bottom left
    { 1.0f, 1.0f, 1.0f, 0.0f }, // top right
    { 0.0f, 1.0f, 1.0f, 0.0f }  // top left
};
/*Vec4 TileFloor::sTexCoords[6] = {
    { 0.0f, 1.0f, 1.0f, 0.0f }, // top left
    { 1.0f, 1.0f, 1.0f, 0.0f }, // top right
    { 0.0f, 0.0f, 1.0f, 0.0f }, // bottom left
    
    { 1.0f, 1.0f, 1.0f, 0.0f }, // top right
    { 0.0f, 0.0f, 1.0f, 0.0f }, // bottom left
    { 1.0f, 0.0f, 1.0f, 0.0f }  // bottom right
};*/

VertexBuffer TileFloor::sVertBuf;

TileFloor::TileFloor() :
    mPosition(0.0f, 0.0f, 0.0f, 1.0f),
    mScale(1.0f, 1.0f, 1.0f, 1.0f)/*,
    mTexture(nullptr)*/
{
    sVertBuf.Init(
        (float*)sVertices,
        nullptr,
        nullptr,
        (float*)sTexCoords,
        6,
        nullptr,
        0
    );
}

TileFloor::~TileFloor()
{
}

void TileFloor::Draw(
    Mat4& viewMat,
    Renderer& render)
{
    //if (mTexture == nullptr) {
    //    printf("TileFloor draw tex is null\n");
    //    return;
    //}
    // assumes all scale components are equal
    const float tileSize = 2.0f * mScale.x;
    //const float tileSize = TILESIZE;
    float x = mPosition.x;
    float z = mPosition.z;
    const size_t numTilesX = 8;
    const size_t numTilesZ = 8;
    Mat4 modelMat;
    
    // TODO
    //render.SetTexture(*mTexture);
    for (size_t i = 0; i < numTilesX; ++i)
    {
        z = mPosition.z;
        for (size_t j = 0; j <numTilesZ; ++j)
        {
            //Vec4 tilePos(x, mPosition.y, z, 1.0f);
            // currently SRT, TODO make TRS
            modelMat = Math::Scale(mScale.x, 1.0f, mScale.z) *
                Math::Translate(x, mPosition.y, z);
            render.DrawVertexBuffer(modelMat, viewMat, sVertBuf);
            
            z += tileSize;
        }
        x += tileSize;
    }
}

} // namespace PS2



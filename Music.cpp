#include <Music.h>

#include <stdlib.h>
#include <stdio.h>

#include <kernel.h>
#include <sifrpc.h>
#include <loadfile.h>
#include <tamtypes.h>

namespace PS2
{

Music::Music() :
    mSampleData(nullptr)
{
}

Music::~Music()
{
    if (mSampleData != nullptr) {
        printf("Freeing sample data\n");
        free(mSampleData);
        mSampleData = nullptr;
    }
}

bool Music::Load(const char* fileName)
{
    FILE* fp = fopen(fileName, "rb");
    if (!fp) {
        printf("Failed to open %s\n", fileName);
        return false;
    }

    size_t fileSize = 0;
    fseek(fp, 0, SEEK_END);
    fileSize = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    mSampleData = (char*)malloc(fileSize);
    if (!mSampleData) {
        printf("Failed to alloc music sample data\n");
        return false;
    }
    
    fread((void*)mSampleData, fileSize, 1, fp);

    audsrv_load_adpcm(&mSample, (unsigned char*)mSampleData, fileSize);

    fclose(fp);
    return true;
}

bool Music::Play(const bool looping)
{
    audsrv_ch_play_adpcm(0, &mSample);
    return true;
}

} // namespace PS2


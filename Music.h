#ifndef PS2_MUSIC_H_INCLUDED
#define PS2_MUSIC_H_INCLUDED

#include <audsrv.h>

namespace PS2
{

class Music
{
public:

    Music();
    ~Music();

    bool Load(const char* fileName);
    bool Play(const bool looping);

private:
    audsrv_adpcm_t mSample;
    char* mSampleData;
};

}

#endif // PS2_MUSIC_H_INCLUDED


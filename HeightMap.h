#ifndef PS2_HEIGHTMAP_H_INCLUDED
#define PS2_HEIGHTMAP_H_INCLUDED

#include <VertexBuffer.h>
#include <Texture.h>
#include <Renderer.h>

namespace PS2
{

class HeightMap
{
public:

	HeightMap();
	~HeightMap();

	bool Load(
		const char* rawFileName,
		const unsigned int width = 65,
		const float heightScale = 10.0f,
		const float sizeScale = 1.0f
	);

    void Draw(
        Mat4& modelMat,
        Mat4& viewMat,
        Renderer& render    
    )
    {
        render.DrawVertexBuffer(modelMat, viewMat, mVertBuff);
    }

private:

	Vec4* mPositions;
    size_t mPositionsSize;
	float* mHeights;
    size_t mHeightsSize;
	Vec4* mTexCoords;
    size_t mTexCoordsSize;
	int* mIndices;
    size_t mIndicesSize;

    int mWidth;

	VertexBuffer mVertBuff;

	bool GenerateVertices(const float sizeScale);
	bool GenerateTexCoords();
	bool GenerateIndices();
	bool GenerateBuffers();
};

} // namespace PS2

#endif // PS2_HEIGHTMAP_H_INCLUDED


#ifndef PS2_TEXTURE_H_INCLUDED
#define PS2_TEXTURE_H_INCLUDED

class Texture
{

friend class Renderer;

public:

    Texture();
    ~Texture();

    // Init from raw data
    bool Init(const char* data, const int numChannels, int width, int height);

    // Load from a TARGA image
    bool LoadFromTarga(const char* fileName);
    
private:
    char* mData;
    int mWidth;
    int mHeight;
    int mNumChannels;
};

#endif


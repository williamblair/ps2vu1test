#ifndef RENDERER_H_INCLUDED
#define RENDERER_H_INCLUDED

#include <kernel.h>
#include <malloc.h>
#include <tamtypes.h>
#include <gs_psm.h>
#include <dma.h>
#include <packet2.h>
#include <packet2_utils.h>
#include <graph.h>
#include <draw.h>

#include <VertexBuffer.h>
#include "Math.h"
#include <Texture.h>

class Renderer
{
public:
    Renderer();
    ~Renderer();

    bool Init();

    void DrawVertexBuffer(Mat4& modelMat, Mat4& viewMat, PS2::VertexBuffer& vb);
    void Update() { graph_wait_vsync(); clear_screen(&frame, &z); }

    // assumes Texture is 128x128 RGB24
    void SetTexture(Texture& tex)
    {
        if (curTexData != tex.mData)
        {
            curTexData = tex.mData;
            send_texture(&texbuff, (const char*)tex.mData);
        }
    }

    void DrawTexture(Texture& tex, int x, int y, int w, int h)
    {
        MATRIX modelMat; // identity matrices
        MATRIX viewMat;
        MATRIX projMat;

        matrix_unit(modelMat);
        matrix_unit(viewMat);
        matrix_unit(projMat);
        
        SetTexture(tex);
        
        // disable z-buffering
        // TODO - not create packet each time
        /*packet2_t* zbufPacket = packet2_create(35, P2_TYPE_NORMAL, P2_MODE_NORMAL, 0);
        packet2_update(zbufPacket, draw_disable_tests(zbufPacket->next, 0, &z));
        packet2_update(zbufPacket, draw_finish(zbufPacket->next));
        dma_wait_fast();
        dma_channel_send_packet2(zbufPacket, DMA_CHANNEL_GIF, 1);
        draw_wait_finish();*/

        float xNorm = (float)x / 640.0f;
        float yNorm = ((float)y / 512.0f);
        float wNorm = (float)w / 640.0f;
        float hNorm = (float)h / 512.0f;
        float leftX = -0.156f + ((float)xNorm)*(0.156f*2.0f); // estimated numbers...
        float bottomY = -0.125f + ((float)yNorm)*(0.125f*2.0f);
        float rightX = leftX + (((float)wNorm)*(0.156f*2.0f));
        float topY = bottomY + (((float)hNorm)*(0.125f*2.0f));
        
        // put vertex data in zbyszek_packet
        // TODO - not create/set vectors each time
        int num_faces = 6;
        *((Vec4*)c_verts[0]) = Vec4( leftX, bottomY, 0.0f, 1.0f ); // bottom left
        *((Vec4*)c_verts[1]) = Vec4(  rightX, bottomY, 0.0f, 1.0f ); // bottom right
        *((Vec4*)c_verts[2]) = Vec4(  rightX,  topY, 0.0f, 1.0f ); // top right
        *((Vec4*)c_verts[3]) = Vec4( leftX, bottomY, 0.0f, 1.0f ); // bottom left
        *((Vec4*)c_verts[4]) = Vec4(  rightX,  topY, 0.0f, 1.0f ); // top right
        *((Vec4*)c_verts[5]) = Vec4( leftX,  topY, 0.0f, 1.0f ); // top left
        
        *((Vec4*)c_sts[0]) = Vec4( 0.0f, 0.0f, 1.0f, 0.0f ); // bottom left
        *((Vec4*)c_sts[1]) = Vec4( 1.0f, 0.0f, 1.0f, 0.0f ); // bottom right
        *((Vec4*)c_sts[2]) = Vec4( 1.0f, 1.0f, 1.0f, 0.0f ); // top right
        *((Vec4*)c_sts[3]) = Vec4( 0.0f, 0.0f, 1.0f, 0.0f ); // bottom left
        *((Vec4*)c_sts[4]) = Vec4( 1.0f, 1.0f, 1.0f, 0.0f ); // top right
        *((Vec4*)c_sts[5]) = Vec4( 0.0f, 1.0f, 1.0f, 0.0f ); // top left
        packet2_reset(zbyszek_packet, 0);
        calculate_cube(&texbuff, num_faces);
        
        // draw via VIF packet
        draw_cube(projMat, viewMat, modelMat, &texbuff, num_faces);
        
        // reenable zbuffering
        /*packet2_reset(zbufPacket, 0);
        packet2_update(zbufPacket, draw_enable_tests(zbufPacket->next, 0, &z));
        packet2_update(zbufPacket, draw_finish(zbufPacket->next));
        dma_wait_fast();
        dma_channel_send_packet2(zbufPacket, DMA_CHANNEL_GIF, 1);
        draw_wait_finish();
        packet2_free(zbufPacket);*/
    }

private:
    MATRIX view_screen;
    MATRIX local_screen;
    
    // Object tmp position
    VECTOR c_zbyszek_position;

    /* Buffers for GS/drawing */
    framebuffer_t frame;
    zbuffer_t z;
    texbuffer_t texbuff;

    char* curTexData;

    /** Cube data
     * TODO - remove, make generic, or make part of vertexbuffer */
    packet2_t *zbyszek_packet;
    
    /** 
     * Packets for sending VU data 
     * Each packet will have: 
     * a) View/Projection matrix (calculated every frame) 
     * b) Cube data (prim,lod,vertices,sts,...) added from zbyszek_packet. 
     */
    packet2_t *vif_packets[2] __attribute__((aligned(64)));
    packet2_t *curr_vif_packet;

    u8 context = 0;
    
    /* Set GS primitive type of drawing */
    prim_t prim;

    /* Color look up table and level of details, needed for texture */
    clutbuffer_t clut;
    lod_t lod;

    /* Helper arrays for calculations */
    VECTOR* c_verts __attribute__((aligned(128)));
    VECTOR* c_sts __attribute__((aligned(128)));

    void init_gs(framebuffer_t *t_frame, zbuffer_t *t_z, texbuffer_t *t_texbuff);
    void init_drawing_environment(framebuffer_t *t_frame, zbuffer_t *t_z);
    void send_texture(texbuffer_t *texbuf, const char* data);
    void clear_screen(framebuffer_t *frame, zbuffer_t *z);
    void set_lod_clut_prim_tex_buff(texbuffer_t *t_texbuff);
    void vu1_set_double_buffer_settings();
    void vu1_upload_micro_program();
    void calculate_cube(texbuffer_t *t_texbuff, int faces_count);
    void draw_cube(
        MATRIX& projMat,
        MATRIX& viewMat,
        MATRIX& modelMat,
        texbuffer_t *t_texbuff,
        int faces_count
    );
};

#endif // RENDERER_H_INCLUDED


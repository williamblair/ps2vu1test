#include <Renderer.h>
#include <sifrpc.h>
#include <sbv_patches.h>
#include <loadfile.h>
#include <audsrv.h>

#include <stdio.h>
#include <string.h>

// IRX modules
#include "audsrv_irx.h"
#include "cdfs.h"
//#include "data/freesd.h"


// TODO - configure
#define MAX_FACES_COUNT 25000

/** Data of our texture (24bit, RGB8) */
//extern unsigned char zbyszek[];

/** 
 * Data of VU1 micro program (draw_3D.vcl/vsm). 
 * How we can use it: 
 * 1. Upload program to VU1. 
 * 2. Send calculated local_screen matrix once per mesh (3D object) 
 * 3. Set buffers size. (double-buffering described below) 
 * 4. Send packet with: lod, clut, tex buffer, scale vector, rgba, verts and sts. 
 * What this program is doing? 
 * 1. Load local_screen. 
 * 2. Zero clipping flag. 
 * 3. Set current buffer start address from TOP register (xtop command) 
 *      To use pararelism, we set two buffers in the VU1. It means, that when 
 *      VU1 is working with one verts packet, we can load second one into another buffer. 
 *      xtop command is automatically switching buffers. I think that AAA games used 
 *      quad buffers (TOP+TOPS) which can give best performance and no VIF_FLUSH should be needed. 
 * 4. Load rest of data. 
 * 5. Prepare GIF tag. 
 * 6. For every vertex: transform, clip, scale, perspective divide. 
 * 7. Send it to GS via XGKICK command. 
 */
extern u32 VU1Draw3D_CodeStart __attribute__((section(".vudata")));
extern u32 VU1Draw3D_CodeEnd __attribute__((section(".vudata")));

Renderer::Renderer() :
    curTexData(nullptr)
{}

Renderer::~Renderer()
{
    // TODO
    //packet2_free(vif_packets[0]);
    //packet2_free(vif_packets[1]);
    //packet2_free(zbyszek_packet);
}

bool Renderer::Init()
{
    // TODO - put this in something like platforminit instead
    int ret = 0;
    int irxRet = 0;

    SifInitRpc( 0 );

    // enable load modules from EE ram
    // PCSX2 requires this; fails on hardware but loads still work
    if (strcmp(ASSETS_DIR, "cdfs:") == 0) {
        ret = sbv_patch_enable_lmb();
        if ( ret != 0 ) {
            printf( "Load Module patch failed\n");
            return 1;
        }
    }
    
    printf( "Ps2 Platform Init Loading LIBSD\n" );
    ret = SifLoadModule( "rom0:LIBSD", 0, NULL );
    //ret = SifExecModuleBuffer(
    //    freesd, // EE RAM buffer to load IRX module from
    //    size_freesd, // size of EE RAM buffer
    //    0, // length of arguments list in bytes
    //    nullptr, // arguments list
    //    &irxRet // store IRX return from _start() function
    //);
    printf( "  libsd loadmodule: %d\n", ret );

    //printf( "Ps2 Platform Init Loading SIO2MAN\n" );
    //ret = SifLoadModule( "rom0:SIO2MAN", 0, NULL );
    //if ( ret < 0 ) {
    //    printf( "  Failed to load SIO2MAN\n" );
    //    return false;
    //}
    //printf( "  sio2man ret: %d\n", ret );

    //printf( "Ps2 Platform Init loading MCMAN\n" );
    //ret = SifLoadModule( "rom0:MCMAN", 0, NULL );
    //if ( ret < 0 ) {
    //    printf( "Failed to load XMCMAN\n" );
    //    return false;
   // }
    //printf( "  MCMAN ret: %d\n", ret );

    //printf( "Ps2 Platform Init loading MCSERV\n" );
    //ret = SifLoadModule( "rom0:MCSERV", 0, NULL );
    //if ( ret < 0 ) {
    //    printf( "Failed to load MCSERV\n" );
    //    return false;
    //}



    printf( "Ps2 Platform Init Loading audsrv.irx buffer\n" );
    irxRet = 0;
    ret = SifExecModuleBuffer(
        audsrv_irx, // EE RAM buffer to load IRX module from
        size_audsrv_irx, // size of EE RAM buffer
        0, // length of arguments list in bytes
        nullptr, // arguments list
        &irxRet // store IRX return from _start() function
    );
    printf( "  audsrv loadmodule %d, IRX _start() ret %d\n", ret, irxRet );
    ret = audsrv_init();
    if ( ret != 0 ) {
        printf( "Failed to init audsrv\n" );
        printf( "%s\n", audsrv_get_error_string() );
        return false;
    }

    printf("audsrv adpcm init\n");
    audsrv_adpcm_init();
    audsrv_set_volume( MAX_VOLUME );
    audsrv_adpcm_set_volume( 0, MAX_VOLUME );
    
    // Init cdfs support
    printf("Loading cdfs\n");
    irxRet = 0;
    ret = SifExecModuleBuffer(
        cdfs_irx,
        size_cdfs_irx,
        0,
        nullptr,
        &irxRet
    );
    printf("  cdfs ret, irx ret: %d, %d\n", ret, irxRet);

    
    // Init DMA channels
    dma_channel_initialize(DMA_CHANNEL_GIF, NULL, 0);
    dma_channel_initialize(DMA_CHANNEL_VIF1, NULL, 0);
    dma_channel_fast_waits(DMA_CHANNEL_GIF);
    dma_channel_fast_waits(DMA_CHANNEL_VIF1);

    // Initialize vif packets
    // arg 0 = maximum data size in qwords (128bit)
    zbyszek_packet = packet2_create(10, P2_TYPE_NORMAL, P2_MODE_CHAIN, 1);
    vif_packets[0] = packet2_create(11, P2_TYPE_NORMAL, P2_MODE_CHAIN, 1);
    vif_packets[1] = packet2_create(11, P2_TYPE_NORMAL, P2_MODE_CHAIN, 1);
    // Testing...
    //zbyszek_packet = packet2_create(4000, P2_TYPE_NORMAL, P2_MODE_CHAIN, 1);
    //vif_packets[0] = packet2_create(4000, P2_TYPE_NORMAL, P2_MODE_CHAIN, 1);
    //vif_packets[1] = packet2_create(4000, P2_TYPE_NORMAL, P2_MODE_CHAIN, 1);

    // Initialize VU1
    vu1_upload_micro_program();
    vu1_set_double_buffer_settings();

    // Init the GS, framebuffer, zbuffer, and texture buffer
    init_gs(&frame, &z, &texbuff);

    // Init the drawing environment and framebuffer
    init_drawing_environment(&frame, &z);

    // Load texture into vram
    //send_texture(&texbuff);

    // render(&frame, &z, &texbuff)
    {
        framebuffer_t* t_frame = &frame;
        zbuffer_t* t_z = &z;
        texbuffer_t* t_texbuff = &texbuff;
        set_lod_clut_prim_tex_buff(t_texbuff);
        /** 
         * Allocate some space for object position calculating. 
         * c_ prefix = calc_
         */
        c_verts = (VECTOR *)memalign(128, sizeof(VECTOR) * MAX_FACES_COUNT);
        c_sts = (VECTOR *)memalign(128, sizeof(VECTOR) * MAX_FACES_COUNT);
        if (!c_verts) {
            printf("Failed to alloc work verts\n");
            return false;
        }
        if (!c_sts) {
            printf("Failed to alloc work sts\n");
            return false;
        }
        //for (int i = 0; i < faces_count; i++)
        //{
        //    c_verts[i][0] = vertices[faces[i]][0];
        //    c_verts[i][1] = vertices[faces[i]][1];
        //    c_verts[i][2] = vertices[faces[i]][2];
        //    c_verts[i][3] = vertices[faces[i]][3];

        //    c_sts[i][0] = sts[faces[i]][0];
        //    c_sts[i][1] = sts[faces[i]][1];
        //    c_sts[i][2] = sts[faces[i]][2];
        //    c_sts[i][3] = sts[faces[i]][3];
        //}

        // Create the view screen matrix
        create_view_screen(view_screen, graph_aspect_ratio(), -3.0f, 3.0f, -3.0f, 3.0f, 1.0f, 2000.0f);
        //calculate_cube(t_texbuff);
        
        // clear the screen
        clear_screen(t_frame, t_z);
    }

    return true;
}

/** Some initialization of GS and VRAM allocation */
void Renderer::init_gs(framebuffer_t *t_frame, zbuffer_t *t_z, texbuffer_t *t_texbuff)
{
    // Define a 32-bit 640x512 framebuffer.
    t_frame->width = 640;
    t_frame->height = 512;
    t_frame->mask = 0;
    t_frame->psm = GS_PSM_32;
    t_frame->address = graph_vram_allocate(t_frame->width, t_frame->height, t_frame->psm, GRAPH_ALIGN_PAGE);

    // Enable the zbuffer.
    t_z->enable = DRAW_ENABLE;
    t_z->mask = 0;
    t_z->method = ZTEST_METHOD_GREATER_EQUAL;
    t_z->zsm = GS_ZBUF_32;
    t_z->address = graph_vram_allocate(t_frame->width, t_frame->height, t_z->zsm, GRAPH_ALIGN_PAGE);

    // Allocate some vram for the texture buffer
    t_texbuff->width = 128;
    t_texbuff->psm = GS_PSM_24;
    t_texbuff->address = graph_vram_allocate(128, 128, GS_PSM_24, GRAPH_ALIGN_BLOCK);

    // Initialize the screen and tie the first framebuffer to the read circuits.
    graph_initialize(t_frame->address, t_frame->width, t_frame->height, t_frame->psm, 0, 0);
}

/** Some initialization of GS 2 */
void Renderer::init_drawing_environment(framebuffer_t *t_frame, zbuffer_t *t_z)
{
    packet2_t *packet2 = packet2_create(20, P2_TYPE_NORMAL, P2_MODE_NORMAL, 0);

    // This will setup a default drawing environment.
    packet2_update(packet2, draw_setup_environment(packet2->next, 0, t_frame, t_z));

    // Now reset the primitive origin to 2048-width/2,2048-height/2.
    packet2_update(packet2, draw_primitive_xyoffset(packet2->next, 0, (2048 - 320), (2048 - 256)));

    // Finish setting up the environment.
    packet2_update(packet2, draw_finish(packet2->next));

    // Now send the packet, no need to wait since it's the first.
    dma_channel_send_packet2(packet2, DMA_CHANNEL_GIF, 1);
    dma_wait_fast();

    packet2_free(packet2);
}

/** Send texture data to GS.
 * assumes data is 128x128px RGB24 */
void Renderer::send_texture(texbuffer_t *texbuf, const char* data)
{
    // TODO - not allocate each texture send
    packet2_t *packet2 = packet2_create(50, P2_TYPE_NORMAL, P2_MODE_CHAIN, 0);
    packet2_update(packet2, draw_texture_transfer(packet2->next, (void*)data, 128, 128, GS_PSM_24, texbuf->address, texbuf->width));
    packet2_update(packet2, draw_texture_flush(packet2->next));
    dma_channel_send_packet2(packet2, DMA_CHANNEL_GIF, 1);
    dma_wait_fast();
    packet2_free(packet2);
}

/** Send packet which will clear our screen. */
void Renderer::clear_screen(framebuffer_t *frame, zbuffer_t *z)
{
    packet2_t *clear = packet2_create(35, P2_TYPE_NORMAL, P2_MODE_NORMAL, 0);

    // Clear framebuffer but don't update zbuffer.
    packet2_update(clear, draw_disable_tests(clear->next, 0, z));
    packet2_update(clear, draw_clear(clear->next, 0, 2048.0f - 320.0f, 2048.0f - 256.0f, frame->width, frame->height, 0x40, 0x40, 0x40));
    packet2_update(clear, draw_enable_tests(clear->next, 0, z));
    packet2_update(clear, draw_finish(clear->next));

    // Now send our current dma chain.
    dma_wait_fast();
    dma_channel_send_packet2(clear, DMA_CHANNEL_GIF, 1);

    packet2_free(clear);

    // Wait for scene to finish drawing
    draw_wait_finish();
}

void Renderer::set_lod_clut_prim_tex_buff(texbuffer_t *t_texbuff)
{
    lod.calculation = LOD_USE_K;
    lod.max_level = 0;
    lod.mag_filter = LOD_MAG_NEAREST;
    lod.min_filter = LOD_MIN_NEAREST;
    lod.l = 0;
    lod.k = 0;

    clut.storage_mode = CLUT_STORAGE_MODE1;
    clut.start = 0;
    clut.psm = 0;
    clut.load_method = CLUT_NO_LOAD;
    clut.address = 0;

    // Define the triangle primitive we want to use.
    prim.type = PRIM_TRIANGLE;
    prim.shading = PRIM_SHADE_GOURAUD;
    prim.mapping = DRAW_ENABLE;
    prim.fogging = DRAW_DISABLE;
    prim.blending = DRAW_ENABLE;
    prim.antialiasing = DRAW_DISABLE;
    prim.mapping_type = PRIM_MAP_ST;
    prim.colorfix = PRIM_UNFIXED;

    t_texbuff->info.width = draw_log2(128);
    t_texbuff->info.height = draw_log2(128);
    t_texbuff->info.components = TEXTURE_COMPONENTS_RGB;
    t_texbuff->info.function = TEXTURE_FUNCTION_DECAL;
}

void Renderer::vu1_set_double_buffer_settings()
{
    packet2_t *packet2 = packet2_create(1, P2_TYPE_NORMAL, P2_MODE_CHAIN, 1);
    packet2_utils_vu_add_double_buffer(packet2, 8, 496);
    packet2_utils_vu_add_end_tag(packet2);
    dma_channel_send_packet2(packet2, DMA_CHANNEL_VIF1, 1);
    dma_channel_wait(DMA_CHANNEL_VIF1, 0);
    packet2_free(packet2);
}

void Renderer::vu1_upload_micro_program()
{
    u32 packet_size =
        packet2_utils_get_packet_size_for_program(&VU1Draw3D_CodeStart, &VU1Draw3D_CodeEnd) + 1; // + 1 for end tag
    packet2_t *packet2 = packet2_create(packet_size, P2_TYPE_NORMAL, P2_MODE_CHAIN, 1);
    packet2_vif_add_micro_program(packet2, 0, &VU1Draw3D_CodeStart, &VU1Draw3D_CodeEnd);
    packet2_utils_vu_add_end_tag(packet2);
    dma_channel_send_packet2(packet2, DMA_CHANNEL_VIF1, 1);
    dma_channel_wait(DMA_CHANNEL_VIF1, 0);
    packet2_free(packet2);
}

/** Calculate packet for cube data */
void Renderer::calculate_cube(texbuffer_t *t_texbuff, int faces_count)
{
    packet2_add_float(zbyszek_packet, 2048.0F);                      // scale
    packet2_add_float(zbyszek_packet, 2048.0F);                      // scale
    packet2_add_float(zbyszek_packet, ((float)0xFFFFFF) / 32.0F); // scale
    packet2_add_s32(zbyszek_packet, faces_count);                  // vertex count
    packet2_utils_gif_add_set(zbyszek_packet, 1);
    packet2_utils_gs_add_lod(zbyszek_packet, &lod);
    packet2_utils_gs_add_texbuff_clut(zbyszek_packet, t_texbuff, &clut);
    packet2_utils_gs_add_prim_giftag(zbyszek_packet, &prim, faces_count, DRAW_STQ2_REGLIST, 3, 0);
    u8 j = 0; // RGBA
    for (j = 0; j < 4; j++)
        packet2_add_u32(zbyszek_packet, 128);
}

void Renderer::DrawVertexBuffer(Mat4& modelMat, Mat4& viewMat, PS2::VertexBuffer& vb)
{
    texbuffer_t* t_texbuff = &texbuff;

    if (vb.mIndicesCount > MAX_FACES_COUNT ||
        vb.mVertexCount > MAX_FACES_COUNT)
    {
        printf("Invalid vertex buffer num indices or vertices %d, %d\n",
            vb.mIndicesCount, vb.mVertexCount);
    }

    // TODO - if check on whether the buffer is indexed or not
    VECTOR* vertices = (VECTOR*)vb.mVertices;
    VECTOR* sts = (VECTOR*)vb.mTexCoords;
    int faces_count = 0;
    if (vb.mIndicesCount > 0)
    {
        //printf("Rendering with indices\n");
        int* faces = vb.mIndices;
        faces_count = vb.mIndicesCount;
        // TODO - possibly store this and zbyszek_packet in VertexBuffer?
        //for (int i = 0; i < faces_count; i++)
        //{
        //    c_verts[i][0] = vertices[faces[i]][0];
        //    c_verts[i][1] = vertices[faces[i]][1];
        //    c_verts[i][2] = vertices[faces[i]][2];
        //    c_verts[i][3] = vertices[faces[i]][3];

        //    c_sts[i][0] = sts[faces[i]][0];
        //    c_sts[i][1] = sts[faces[i]][1];
        //    c_sts[i][2] = sts[faces[i]][2];
        //    c_sts[i][3] = sts[faces[i]][3];
        //}
        int cur_face = 0;
        int faces_remain = faces_count;
        while (faces_remain > 0)
        {
            int num_faces = (faces_remain > 96 ? 96 : faces_remain);
            if (num_faces % 3 != 0) {
                printf("ERROR - num faces not mult of 3\n");
                return;
            }
            for (int i = 0; i < num_faces; ++i)
            {
                c_verts[i][0] = vertices[faces[cur_face]][0];
                c_verts[i][1] = vertices[faces[cur_face]][1];
                c_verts[i][2] = vertices[faces[cur_face]][2];
                c_verts[i][3] = vertices[faces[cur_face]][3];

                c_sts[i][0] = sts[faces[cur_face]][0];
                c_sts[i][1] = sts[faces[cur_face]][1];
                c_sts[i][2] = sts[faces[cur_face]][2];
                c_sts[i][3] = sts[faces[cur_face]][3];

                ++cur_face;
            }
            // see ps2sdk/ee/include/packet2.h
            // 0 = do NOT clear memory
            //printf("packet 2 reset\n");
            packet2_reset(zbyszek_packet, 0);
            //printf("calculate cube\n");
            calculate_cube(t_texbuff, /*faces_count*/num_faces);
            //printf("draw cube\n");
            draw_cube(
                view_screen,
                MATRIXCAST(viewMat),
                MATRIXCAST(modelMat),
                t_texbuff,
                /*faces_count*/num_faces
            );
            
            faces_remain -= num_faces;
        }
    }
    else
    {
        //printf("Rendering with vertices; count: %d\n",
        //    vb.mVertexCount);
        faces_count = vb.mVertexCount;
        //for (int i = 0; i < faces_count; i++)
        //{
        //    c_verts[i][0] = vertices[i][0];
        //    c_verts[i][1] = vertices[i][1];
        //    c_verts[i][2] = vertices[i][2];
        //    c_verts[i][3] = vertices[i][3];

        //    c_sts[i][0] = sts[i][0];
        //    c_sts[i][1] = sts[i][1];
        //    c_sts[i][2] = sts[i][2];
        //    c_sts[i][3] = sts[i][3];
        //}
        
        int cur_face = 0;
        int faces_remain = faces_count;
        while (faces_remain > 0)
        {
            int num_faces = (faces_remain > 96 ? 96 : faces_remain);
            if (num_faces % 3 != 0) {
                printf("ERROR - num faces not mult of 3\n");
                return;
            }
            for (int i = 0; i < num_faces; ++i)
            {
                c_verts[i][0] = vertices[cur_face][0];
                c_verts[i][1] = -vertices[cur_face][1]; // negative because everything's upside down currently
                c_verts[i][2] = vertices[cur_face][2];
                c_verts[i][3] = vertices[cur_face][3];

                c_sts[i][0] = sts[cur_face][0];
                c_sts[i][1] = sts[cur_face][1];
                c_sts[i][2] = sts[cur_face][2];
                c_sts[i][3] = sts[cur_face][3];

                ++cur_face;
            }
            // see ps2sdk/ee/include/packet2.h
            // 0 = do NOT clear memory
            //printf("packet 2 reset\n");
            packet2_reset(zbyszek_packet, 0);
            //printf("calculate cube\n");
            calculate_cube(t_texbuff, /*faces_count*/num_faces);
            //printf("draw cube\n");
            draw_cube(
                view_screen,
                MATRIXCAST(viewMat),
                MATRIXCAST(modelMat),
                t_texbuff,
                /*faces_count*/num_faces
            );
            
            faces_remain -= num_faces;
        }
    }

    // test upload texture...
    //send_texture(t_texbuff);

    // see ps2sdk/ee/include/packet2.h
    // 0 = do NOT clear memory
    //printf("packet 2 reset\n");
    //packet2_reset(zbyszek_packet, 0);
    //printf("calculate cube\n");
    //calculate_cube(t_texbuff, faces_count);
    //printf("draw cube\n");
    //draw_cube(
    //    MATRIXCAST(viewMat),
    //    MATRIXCAST(modelMat),
    //    t_texbuff,
    //    faces_count
    //);
}


/** Calculate cube position and add packet with cube data */
void Renderer::draw_cube(
    MATRIX& projMat,
    MATRIX& viewMat,
    MATRIX& modelMat,
    texbuffer_t* t_texbuff,
    int faces_count)
{
    //printf("draw cube create local screen\n");
    create_local_screen(local_screen, modelMat, viewMat, projMat);
    curr_vif_packet = vif_packets[context];
    packet2_reset(curr_vif_packet, 0);

    // Add matrix at the beggining of VU mem (skip TOP)
    //printf("draw cube vu add unpacl local screen mat\n");
    packet2_utils_vu_add_unpack_data(curr_vif_packet, 0, &local_screen, 8, 0);

    u32 vif_added_bytes = 0; // zero because now we will use TOP register (double buffer)
                             // we don't wan't to unpack at 8 + beggining of buffer, but at
                             // the beggining of the buffer

    // Merge packets
    //printf("draw cube vu add unpacl zbyszek packet\n");
    packet2_utils_vu_add_unpack_data(curr_vif_packet, vif_added_bytes, zbyszek_packet->base, packet2_get_qw_count(zbyszek_packet), 1);
    vif_added_bytes += packet2_get_qw_count(zbyszek_packet);

    // Add vertices
    //printf("draw cube vu add unpack vertices\n");
    packet2_utils_vu_add_unpack_data(curr_vif_packet, vif_added_bytes, c_verts, faces_count, 1);
    vif_added_bytes += faces_count; // one VECTOR is size of qword

    // Add sts
    //printf("draw cube vu add unpack sts\n");
    packet2_utils_vu_add_unpack_data(curr_vif_packet, vif_added_bytes, c_sts, faces_count, 1);
    vif_added_bytes += faces_count;

    //printf("draw cube vu add start program\n");
    packet2_utils_vu_add_start_program(curr_vif_packet, 0);
    packet2_utils_vu_add_end_tag(curr_vif_packet);
    dma_channel_wait(DMA_CHANNEL_VIF1, 0);
    dma_channel_send_packet2(curr_vif_packet, DMA_CHANNEL_VIF1, 1);

    // Switch packet, so we can proceed during DMA transfer
    context = !context;
}

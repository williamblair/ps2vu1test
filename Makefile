# _____     ___ ____     ___ ____
#  ____|   |    ____|   |        | |____|
# |     ___|   |____ ___|    ____| |    \    PS2DEV Open Source Project.
#-----------------------------------------------------------------------
# Copyright 2001-2004, ps2dev - http://www.ps2dev.org
# Licenced under Academic Free License version 2.0
# Review ps2sdk README & LICENSE files for further details.

EE_BIN = vu1.elf
EE_OBJS = draw_3D.o \
          main.o \
          Renderer.o \
          Camera.o \
          MoveComponent.o \
          Controller.o \
          VertexBuffer.o \
          TileFloor.o \
          Texture.o \
          Md2Model.o \
          ThirdPersonPlayer.o \
          Music.o \
          HeightMap.o
EE_LIBS = -ldraw -lgraph -lmath3d -lpacket2 -ldma -lpad -lpatches -laudsrv
EE_DVP = dvp-as
EE_CXXFLAGS := -DASSETS_DIR=\"host:\"
#EE_CXXFLAGS := -DASSETS_DIR=\"cdfs:\"
#EE_VCL = vcl
#EE_VCL = openvcl

all: zbyszek.c $(EE_BIN)
	$(EE_STRIP) --strip-all $(EE_BIN)

# Original VCL tool preferred. 
# It can be runned on WSL, but with some tricky commands: 
# https://github.com/microsoft/wsl/issues/2468#issuecomment-374904520
#%.vsm: %.vcl
#	$(EE_VCL) $< >> $@

%.o: %.vsm
	$(EE_DVP) $< -o $@

zbyszek.c:
	bin2c zbyszek.raw zbyszek.c zbyszek

clean:
	rm -f $(EE_BIN) $(EE_OBJS) zbyszek.c

run: $(EE_BIN)
	ps2client -h 192.168.0.7 execee host:$(EE_BIN)

reset:
	ps2client reset

iso:
	rm -rf cdrom
	mkdir cdrom
	cp SYSTEM.CNF cdrom
	cp $(EE_BIN) cdrom/MAIN.ELF
	cp -r data cdrom
	mkisofs -o vu1bj.iso cdrom

include $(PS2SDK)/samples/Makefile.pref
include $(PS2SDK)/samples/Makefile.eeglobal_cpp

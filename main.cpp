
#include <Renderer.h>
#include "Math.h"
#include <Camera.h>
#include <GameTimer.h>
#include <Controller.h>
#include <VertexBuffer.h>
#include <TileFloor.h>
#include <Texture.h>
#include <Md2Model.h>
#include <ThirdPersonPlayer.h>
#include <Music.h>
#include <HeightMap.h>

#include "zbyszek.c"
//#include "mesh_data.c"

static Renderer gRender;
//static PS2::FPSCamera gCamera;
static PS2::GameTimer gTimer;
static PS2::Controller gPad;
//static PS2::TileFloor gFloor;
static PS2::Md2Model gMd2;
//static PS2::ThirdPersonPlayer gPlayer;
static PS2::FPSCamera gCamera;
static PS2::Music gMusic;
static PS2::HeightMap gHeightMap;

//static Texture gZbyszekTex;
static Texture gGrassTex;
static Texture gMd2Tex;

#if 1
void MoveCamera(const float dt)
{
    const float moveSpeed = 50.0f;
    if (gPad.GetLeftJoyY() < 50) {
        gCamera.GetMoveComponent().MoveForward(moveSpeed * dt);
    }
    if (gPad.GetLeftJoyY() > 200) {
        gCamera.GetMoveComponent().MoveForward(-moveSpeed * dt);
    }

    if (gPad.GetLeftJoyX() < 50) {
        gCamera.GetMoveComponent().MoveRight(-moveSpeed * dt);
    }
    if (gPad.GetLeftJoyX() > 200) {
        gCamera.GetMoveComponent().MoveRight(moveSpeed * dt);
    }

    const float rotSpeed = 30.0f;
    if (gPad.GetRightJoyX() < 50) {
        gCamera.GetMoveComponent().AddYaw(rotSpeed * dt);
    }
    if (gPad.GetRightJoyX() > 200) {
        gCamera.GetMoveComponent().AddYaw(-rotSpeed * dt);
    }
    if (gPad.GetRightJoyY() < 50) {
        gCamera.GetMoveComponent().AddPitch(-rotSpeed * dt);
    }
    if (gPad.GetRightJoyY() > 200) {
        gCamera.GetMoveComponent().AddPitch(rotSpeed * dt);
    }
}
#endif

#if 0
static void MovePlayer(const float dt)
{
    int x = gPad.GetLeftJoyX();
    int y = gPad.GetLeftJoyY();
    float xNorm = ((float)(x - 127)) / 127.0f;
    float yNorm = ((float)(y - 127)) / 127.0f;
    const float moveSpeed = 30.0f;
    gPlayer.Move(-xNorm, yNorm, moveSpeed * dt);
}

static void RotateCamera(const float dt)
{
    PS2::OrbitCamera& oc = gPlayer.GetCamera();
    const float rotSpeed = 16.0f;
    if (gPad.GetRightJoyX() < 50) {
        oc.AddYaw(rotSpeed * dt);
    }
    if (gPad.GetRightJoyX() > 200) {
        oc.AddYaw(-rotSpeed * dt);
    }
    if (gPad.GetRightJoyY() < 50) {
        oc.AddPitch(rotSpeed * dt);
    }
    else if (gPad.GetRightJoyY() > 200) {
        oc.AddPitch(-rotSpeed * dt);
    }
}
#endif

int main(int argc, char* argv[])
{
    Mat4 terrainModelMat;
    Mat4 md2ModelMat;

    if (!gRender.Init()) { return 1; }
    if (!gPad.Init(0)) { return 1; }
    //if (!gZbyszekTex.Init((const char*)zbyszek, 3, 128, 128)) { return 1; }
    if (!gMusic.Load(ASSETS_DIR"data/battleA.adp")) { return 1; } 
    if (!gGrassTex.LoadFromTarga(ASSETS_DIR"data/grass.tga")) { return 1; }
    if (!gMd2Tex.LoadFromTarga(ASSETS_DIR"data/Ogro/Ogrobase.tga")) { return 1; }
    if (!gMd2.Load(ASSETS_DIR"data/Ogro/tris.md2", 1.0f)) { return 1; }
    if (!gHeightMap.Load(ASSETS_DIR"data/hghtmap.raw")) { return 1; }
    
    gCamera.GetMoveComponent().position = Vec4(0.0f, -50.0f, 40.0f, 1.0f);
    gCamera.GetMoveComponent().target = Vec4(0.0f, -50.0f, 39.0f, 1.0f);
    gCamera.Update();

    terrainModelMat = Math::Scale(64.0f, 64.0f, 64.0f) *
        Math::Translate(0.0f, 0.0f, 0.0f);
    md2ModelMat = Math::Translate(0.0f, 0.0f, 0.0f);

    /*PS2::VertexBuffer cubeVertBuf;
    cubeVertBuf.Init(
        (float*)vertices,
        nullptr, // normals
        nullptr, // colors
        (float*)sts, // texcoords
        vertex_count,
        faces, // indices
        faces_count
    );*/
    // TODO - reverse matrix mult order
    //Mat4 cubeModelMat = Math::Rotate(0.0f, 0.0f, 0.0f) *
    //    Math::Translate(0.0f, 30.0f, 0.0f);

    gMd2.SetAnimation("stand");
    gMd2.Update(0.0f);
    //gPlayer.SetModel(&gMd2);
    //gPlayer.SetTexture(&gMd2Tex);
    //gPlayer.GetCamera().SetDistance(175.0f);
    //gPlayer.SetDrawYawOffset(90.0f);

#if 0
    {
        // TODO - y component negated again
        Vec4 floorPos(0.0f, 25.0f, 0.0f, 1.0f);
        gFloor.SetScale(20.0f);
        gFloor.SetPosition(floorPos);
    }
#endif

    gPad.Update();
    //gPlayer.Update(0.0f);

    //gMusic.Play(true); // loop

    
    //Texture* curTex = &gZbyszekTex;
    //gRender.SetTexture(gMd2Tex);
    for (;;)
    {
        const float dt = gTimer.Update();

        //if (gPad.Clicked(PAD_CROSS)) {
        //    if (curTex == &gZbyszekTex) { curTex = &gGrassTex; }
        //    else { curTex = &gZbyszekTex; }
        //}

        //gRender.SetTexture(*curTex);
        //gFloor.Draw(gCamera.GetViewMat(), gRender);
        //gFloor.Draw(gPlayer.GetCamera().GetViewMat(), gRender);

        //gRender.SetTexture(gMd2Tex);
        //printf("Rendering md2\n");
        //gMd2.Render(md2ModelMat, gCamera.GetViewMat(), gRender);
        //gPlayer.Draw(gRender);

        gRender.SetTexture(gMd2Tex);
        gMd2.Render(md2ModelMat, gCamera.GetViewMat(), gRender);

        gRender.DrawTexture(gMd2Tex, 50, 50, 100, 100);

        gRender.SetTexture(gGrassTex);
        gHeightMap.Draw(terrainModelMat, gCamera.GetViewMat(), gRender);


        gMd2.Update(dt);
        //MovePlayer(dt);
        //RotateCamera(dt);        
        //gPlayer.Update(dt);
        MoveCamera(dt);

        gCamera.Update();
        gPad.Update();
        gRender.Update();
    }

    SleepThread();
    return 0;
}

